<?php

namespace multilang;

class Leng
{

public static $lang;

public static $memento = array(
			"en" => array(
				"hello" => "Hello",
				"Home" => "HOME",
				"Work" => "Work",
				"About" => "About",
				"Blog" => "Blog",
				"Contact" => "Contact",
				"my_name" => "My name is"
				),
			"ru"=> array(
				"hello" => "Привет",
				"Home" => "ДОМАШНЯЯ",
				"Work" => "Работа",
				"About" => "Обо мне",
				"Blog" => "Блог",
				"Contact" => "Контакты", 
				"my_name" => "Меня зовут"
				),
			"ua"=> array(
				"hello" => "Привіт",
				"Home" => "ДОМАШНЯ",
				"Work" => "Робота",
				"About" => "Про мене",
				"Blog" => "Блог",
				"Contact" => "Контакты", 
				"my_name" => "Меня звати"
			),
		);

	public static function translate($word)
	{
		if (!empty(self::$memento[self::$lang])){
			if(!empty(self::$memento[self::$lang][$word]))
				return self::$memento[self::$lang][$word];
		}
		return $word;
	}

	public static function set_temp_lang($leng){
        self::$lang = $leng;
    }
}
?>