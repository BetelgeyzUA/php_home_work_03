<?php
namespace components;

use multilang\Lengform as MultilengForm;
use multilang\Leng as Multilengleng;

class App
{

    public $_default_lengs;

    public $_app_lengs;

    public $_multi_translate;

    public function __construct()
    {
        
     
        echo(MultilengForm::call_form());

        $this->_default_lengs = new Request();
 
        $this->_app_lengs = $this->_default_lengs->call_leng();

        Multilengleng::set_temp_lang($this->_app_lengs);

        $home = Multilengleng::translate('Home');
        $work = Multilengleng::translate('Work');
        $about = Multilengleng::translate('About');
        $blog = Multilengleng::translate('Blog');
        $contact = Multilengleng::translate('Contact');

        echo ('<nav style = "display: block; width: 660px; margin: 0 0 30px;">');
        echo ('<ul style = "list-style: none; margin: 0 auto; padding: 1em 0; background: #ECDAD6;">');
        echo ("<li style = 'display: inline;'><a href='#' style = 'padding: 1em; background: rgba(177, 152, 145, .3); border-right: 1px solid #b19891; color: #695753; text-decoration: none;'><i style = 'margin-right: 10px;'>$home</i></a></li>");
        echo ("<li style = 'display: inline;'><a href='#' style = 'padding: 1em; background: rgba(177, 152, 145, .3); border-right: 1px solid #b19891; color: #695753; text-decoration: none;'>$work</a></li>");
        echo ("<li style = 'display: inline;'><a href='#' style = 'padding: 1em; background: rgba(177, 152, 145, .3); border-right: 1px solid #b19891; color: #695753; text-decoration: none;'>$about</a></li>");
        echo ("<li style = 'display: inline;'><a href='#' style = 'padding: 1em; background: rgba(177, 152, 145, .3); border-right: 1px solid #b19891; color: #695753; text-decoration: none;'>$blog</a></li>");
        echo ("<li style = 'display: inline;'><a href='#' style = 'padding: 1em; background: rgba(177, 152, 145, .3); border-right: 1px solid #b19891; color: #695753; text-decoration: none;'>$contact</a></li>");
        echo ('</ul>');
        echo ('</nav');
    }


    /**
     * @return int
     */
    public function run()
    {
        
        echo '<br>-----------------------------------------------<br>';
        return 0;
    }

}
?>
