<?php

namespace components;

class Request
{
	
	protected $lengs;
	
	public function __construct()
	{
		
		$this->lengs = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
	}

	public function call_leng() {
		

        if(isset($_GET['lengs'])) {
           $this->lengs = $_GET['lengs'];
        }

		return "$this->lengs";
	}

}

?>